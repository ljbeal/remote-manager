import os
import glob
import shutil


def _wipe_testing_areas(clear_coverage: bool = False):

    if clear_coverage:
        # remove any lingering --cov-append files
        for file in glob.glob('.coverage*'):
            if file != '.coveragerc':
                os.remove(file)
                print(f'removed coverage file {file}')

    rundir = os.getcwd()

    for root, dirs, files in os.walk(rundir):

        if 'tests' not in root and 'tutorials' not in root:
            continue

        for dir in dirs:
            if dir.startswith('temp_'):
                fullpath = os.path.join(root, dir)

                print(f'removing dir {fullpath}')

                shutil.rmtree(fullpath)

        for file in files:
            if file.startswith('temp_') or file.startswith('dataset-'):
                fullpath = os.path.join(root, file)

                print(f'removing file {fullpath}')
                os.remove(fullpath)


def pytest_sessionstart(session):
    """
    Called after the Session object has been created and
    before performing collection and entering the run test loop.

    Important to clean out any testing folders
    """
    print('Pre-testing setup begins')
    _wipe_testing_areas(clear_coverage=True)


def pytest_sessionfinish(session, exitstatus):
    """
    Clear out folders after the testing sessions finishes
    """
    print('\nPost-testing cleanup begins')
    _wipe_testing_areas()


def pytest_collectstart(collector):
    if collector.fspath and collector.fspath.ext == '.ipynb':
        collector.skip_compare += 'text/html', \
                                  'application/javascript', \
                                  'stderr',


if __name__ == '__main__':
    print('testing pytest init')
    pytest_sessionstart(None)
