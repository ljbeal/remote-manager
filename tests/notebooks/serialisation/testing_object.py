import numpy as np

test = {'string': 'str',
        'int': 42,
        'float': 3.14,
        'list': ['a', 'b', 'c'],
        'tuple': ('a', 'b', 'c'),
        'set': {'a', 'b', 'c'},
        'dict': {'val': 'value'},
        'edict': {},
        'numpy_vals': [np.int32(10), np.float32(7.7)]
       }
