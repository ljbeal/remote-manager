.. remotemanager documentation master file, created by
   sphinx-quickstart on Tue Jun  7 09:51:53 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to remotemanager's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

``remotemanager`` is a flexible and modular package dedicated to facilitating
running python functions on remote systems.

See the introduction page covering the very basics:

.. toctree::
   :maxdepth: 1
   :caption: Introduction

   Introduction

Or see the tutorials for usage help:

.. toctree::
   :maxdepth: 1
   :caption: Tutorials

   tutorials/Quickstart
   tutorials/Logging Usage
   tutorials/Verbosity
   tutorials/URL Usage
   tutorials/Dataset Usage
   tutorials/Removing Attributes
   tutorials/Submitting Via Scheduler
   tutorials/Creating a Dedicated Computer Class
   tutorials/Chaining Datasets
   tutorials/Transport Usage
   tutorials/Flags
   tutorials/Jupyter Magic
   tutorials/Extra Functions

.. toctree::
   :maxdepth: 2
   :caption: API Documentation

   remotemanager

Alternatively, see the :ref:`modindex` for full docstrings and source code


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
