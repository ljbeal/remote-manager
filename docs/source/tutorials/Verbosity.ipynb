{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "e2b035bc-71fa-4769-b489-634fd87272dd",
   "metadata": {},
   "source": [
    "# Verbose levels\n",
    "\n",
    "For user interfacing, the important objects within remotemanager have a \"verbose\" object, which connects with the Logger. This means that whenever a message would be logged, if the appropriate verbose level is set, that message will also be printed. \n",
    "\n",
    "When setting `verbose`, note that print statments will be made for anything _above_ and including that level.\n",
    "\n",
    "For example, setting `verbose=7` means that only critical errors get printed.\n",
    "\n",
    "Likewise, setting `verbose=1` will print everything that is logged.\n",
    "\n",
    "Set to 0 for absolutely no output.\n",
    "\n",
    "The table below is in order of increasing \"verbosity\": Each row prints more than the last.\n",
    "\n",
    "+----------+-------------------------+---------------+\n",
    "| Mode     | Logs and Displays       | Verbose Level |\n",
    "+==========+=========================+===============+\n",
    "| Disabled | Nothing                 | 0             |\n",
    "+----------+-------------------------+---------------+\n",
    "| Critical | Fatal errors only       | 7             |\n",
    "+----------+-------------------------+---------------+\n",
    "| Error    | Damaging errors         | 6             |\n",
    "+----------+-------------------------+---------------+\n",
    "| Warning  | All errors and warnings | 5             |\n",
    "+----------+-------------------------+---------------+\n",
    "| Important| Noteworthy non-errors   | 4 (Default)   |\n",
    "+----------+-------------------------+---------------+\n",
    "| Info     | Important runtime info  | 3             |\n",
    "+----------+-------------------------+---------------+\n",
    "| Runtime  | Runtime information     | 2             |\n",
    "+----------+-------------------------+---------------+\n",
    "| Debug    | Everything              | 1             |\n",
    "+----------+-------------------------+---------------+\n",
    "\n",
    "This numbering system follows the convention of the logging levels available in the python `logging` module, with the exception that two more levels (Runtime and Important) have been added.\n",
    "\n",
    ".. note::\n",
    "    The `verbose` option is available for Dataset, URL and Runner (via `append_run`). If not passed to URL or Runner, they will inherit the level of Dataset.\n",
    "\n",
    "#### Default level demonstration\n",
    "\n",
    "This will be easiest to demonstrate using a Dataset, which defaults to level 4 (Important):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "c29e7a18-144a-4ab9-be0c-391e1e52149e",
   "metadata": {},
   "outputs": [],
   "source": [
    "from remotemanager import Dataset\n",
    "\n",
    "def f(inp):\n",
    "    return f'inp was {inp}'\n",
    "\n",
    "ds = Dataset(f, block_reinit=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "c7488205-d3a3-426d-b26b-11960f19cfbd",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "appended run runner-0\n",
      "appended run runner-1\n"
     ]
    }
   ],
   "source": [
    "ds.append_run(args={'inp': 'first input'})\n",
    "ds.append_run(args={'inp': 'second input'})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "376b393c-831f-4b34-a86e-61cdb1a0940e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "assessing run for runner dataset-0ae54639-runner-0... checks passed, running\n",
      "assessing run for runner dataset-0ae54639-runner-1... checks passed, running\n"
     ]
    }
   ],
   "source": [
    "ds.run()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "f90de153-16e1-4f24-a369-33934d6781da",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "assessing run for runner dataset-0ae54639-runner-0... skipping already submitted run\n",
      "assessing run for runner dataset-0ae54639-runner-1... skipping already submitted run\n"
     ]
    }
   ],
   "source": [
    "ds.run()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "bad76230-985b-4054-a15e-9d4b7af141cd",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "checking remotely for finished runs\n"
     ]
    }
   ],
   "source": [
    "ds.fetch_results()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "33e7374f-bf20-46a9-9c91-df074f53a642",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['inp was first input', 'inp was second input']"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ds.results"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a26b4af6-9285-4f51-9838-6da91ddd0b4f",
   "metadata": {},
   "source": [
    "#### Less verbose demonstration\n",
    "\n",
    "Now lets set the verbose to warnings only and repeat the process:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "f2f5f185",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = Dataset(f, verbose = 5, block_reinit=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "24280099",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds.append_run(args={'inp': 'first input'})\n",
    "ds.append_run(args={'inp': 'second input'})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "aa2064f5",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds.run()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "3a567459",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "skipping already submitted run\n",
      "skipping already submitted run\n"
     ]
    }
   ],
   "source": [
    "ds.run()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "e96d939d",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds.fetch_results()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "43315662",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['inp was first input', 'inp was second input']"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ds.results"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18a43739-9102-46d0-b39f-e1a620d4b43f",
   "metadata": {},
   "source": [
    "#### More verbose demonstration\n",
    "\n",
    "And now the opposite, lets lower to `info` to see how much info is printed. \n",
    "\n",
    "Here we will demonstrate how we can also set the verbose level by its name:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "0d387f3e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "dataset initialised\n",
      "uuid is 0ae5463953335c475797c090b2c185e9eeeec38feebe2a25656e86ff701b5e9a\n",
      "new url is being set to None\n",
      "no URL specified for this dataset, creating localhost\n",
      "new url created with url details:\n",
      "   host: localhost\n",
      "   port: None\n",
      "   user: None\n",
      "no transport specified for this dataset, creating basic rsync\n",
      "no serialiser specified,creating basic json\n",
      "No database file found, creating anew\n",
      "Dataset override pack called\n",
      "Dataset dataset init complete\n"
     ]
    }
   ],
   "source": [
    "ds = Dataset(f, verbose = 'info', block_reinit=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "02d4c28f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "new runner (id 23a1f1b02621b8d66ab07d50b4168d3d20a7fa6bd2835d962b235dd75901bf4c) created\n",
      "updating runner 23a1f1b0 state -> created\n",
      "appended run runner-0\n",
      "Dataset override pack called\n",
      "new runner (id d65c9883adc9c156044fa94f5deb468d0275c654ce303b95bcc9a09c85b856f2) created\n",
      "updating runner d65c9883 state -> created\n",
      "appended run runner-1\n",
      "Dataset override pack called\n"
     ]
    }
   ],
   "source": [
    "ds.append_run(args={'inp': 'first input'})\n",
    "ds.append_run(args={'inp': 'second input'})"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8c252759-8511-4705-bcf9-6b9f2f9b33bd",
   "metadata": {},
   "source": [
    "#### Setting for individual objects\n",
    "\n",
    "As was hinted at earlier, we can also set specific levels for URL and Runners. In the following example we provide a \"muted\" URL, but keep the Dataset at `info`, note how the output changes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "5b558ebc-b7e3-4691-8391-527fcb86be07",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "dataset initialised\n",
      "uuid is 0ae5463953335c475797c090b2c185e9eeeec38feebe2a25656e86ff701b5e9a\n",
      "new url is being set to <remotemanager.connection.url.URL object at 0x7f5310279f00>\n",
      "no transport specified for this dataset, creating basic rsync\n",
      "no serialiser specified,creating basic json\n",
      "No database file found, creating anew\n",
      "Dataset override pack called\n",
      "Dataset dataset init complete\n"
     ]
    }
   ],
   "source": [
    "from remotemanager import URL\n",
    "\n",
    "no_verbose_url = URL(verbose=0)\n",
    "\n",
    "ds = Dataset(f, verbose = 'info', url = no_verbose_url, block_reinit=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "4efa88f2-e85f-46d0-ae3d-3a543c10ceac",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "appended run runner-0\n",
      "Dataset override pack called\n",
      "appended run runner-1\n",
      "Dataset override pack called\n"
     ]
    }
   ],
   "source": [
    "ds.append_run(args={'inp': 'first input'}, verbose = 0)\n",
    "ds.append_run(args={'inp': 'second input'}, verbose = 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1784a7f7-c60e-40b9-a8f7-afdd0a32474d",
   "metadata": {},
   "source": [
    "#### Quiet Mode\n",
    "\n",
    "Some Dataset functions have the option to give a `quiet` argument, which mutes any status printing for the duration of that call. This is currently implemented for `append_run`, `run` and `fetch_results`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "b0b6f12f-7f30-4dd0-8d3d-5200904ffb2c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "dataset initialised\n",
      "uuid is 0ae5463953335c475797c090b2c185e9eeeec38feebe2a25656e86ff701b5e9a\n",
      "new url is being set to <remotemanager.connection.url.URL object at 0x7f5310279f00>\n",
      "no transport specified for this dataset, creating basic rsync\n",
      "no serialiser specified,creating basic json\n",
      "No database file found, creating anew\n",
      "Dataset override pack called\n",
      "Dataset dataset init complete\n"
     ]
    }
   ],
   "source": [
    "ds = Dataset(f, verbose = 'info', url = no_verbose_url, block_reinit=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "32cc7936-93dc-46e2-ab44-0532aac280e1",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds.append_run(args={'inp': 'first input'}, quiet = True)\n",
    "ds.append_run(args={'inp': 'second input'}, quiet = True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "dd416728-9e53-47bc-aa5f-227ad89f2123",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds.run(quiet=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "9865b96b-a554-4132-9616-845932b53cdb",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds.fetch_results(quiet=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "47de9b39-2413-45e8-a29b-202c80051a04",
   "metadata": {},
   "source": [
    "#### Distinction between Logger.level, verbose and quiet\n",
    "\n",
    "`Logger.level`, `verbose` and `quiet` are all separate entities. For example, if you have `Logger.level` set to `debug`, and `verbose='warning'`. The logfile will still contain messages at `debug` level, but only logged messages at `warning` or above will be _printed_.\n",
    "\n",
    "`Quiet` simply mutes any printing (not logging) for the duration of that call."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
