{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Advanced URL usage\n",
    "\n",
    "In the quickstart guide, we covered the basic utilities required to run a python function on a remote machine. Most of the legwork here is done by the backround `URL` module, which of course has more functionality than was shown in the previous tutorial.\n",
    "\n",
    "As was mentioned in quickstart, `URL` has a requirement in that you must be able to connect to a remote machine with no further prompts after the initial `ssh ...` input.\n",
    "\n",
    "The reason for this is that `URL` is nothing more than a fancy wrapper for `ssh`, and we shall touch on that later in this tutorial by exposing the actual `ssh` strings.\n",
    "\n",
    "#### Testing a Remote Connection\n",
    "\n",
    "Before using `URL` it is wise to test a basic command. Start with a simple `ssh {user}@{remote} \"ls\"` and see what comes back (changing `user` and `remote` to suit your use-case).\n",
    "\n",
    "For connection difficulties regarding permssions, see the [relevant section](../Introduction.html#Connecting-to-a-Remote-Machine) of the introduction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import remotemanager\n",
    "remotemanager.Logger.path = 'URL_Tutorial'\n",
    "remotemanager.Logger.level = 'debug'\n",
    "\n",
    "from remotemanager import URL"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# use a localhost connection to ensure compatibility\n",
    "connection = URL(host='localhost')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now have the concept of a \"connection\" to a remote machine. Here, we are using a simple \"localhost\" connection, remember that URL is able to connect outside of your current workstation by specifying a user and hostname as follows:\n",
    "\n",
    "`connection = URL(user='username', host='remote.connection.address')`\n",
    "\n",
    "`connection = URL(user='username', host='192.168.123.456')`\n",
    "\n",
    "\n",
    ".. versionadded:: 0.5.10\n",
    "\n",
    "`url` now also provides a `test_connection()` method which will attempt to connect to the remote, run some simple tests and then return the result."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Checking for entry point... Success (/home/test/remotemanager/docs/source/tutorials)\n",
      "Checking file creation in home... True\n",
      "Checking file creation in /tmp... True\n",
      "Checking file creation in /scratch... False\n",
      "Testing remotemanager.transport.rsync:\n",
      "\tsend... True\n",
      "\tpull... True\n",
      "Testing remotemanager.transport.scp:\n",
      "\tsend... True\n",
      "\tpull... True\n",
      "Cleaning up... Done\n",
      "Done! Made 15 calls, taking 0.03s\n",
      "Approximate latency, 0.00s\n",
      "Tests passed successfully\n"
     ]
    }
   ],
   "source": [
    "# NBVAL_IGNORE_OUTPUT\n",
    "connection.test_connection()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The results are returned in dictionary format and can be queried here, or from the `URL` itself:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`test_connection` creates a `ConnectionTest` object and runs the contained tests. Within, we test the minimal required functionality for running jobs:\n",
    "\n",
    "#. connection to the remote\n",
    "#. creation of a file in at least the home dir\n",
    "#. functional transport system\n",
    "\n",
    "Provided these three conditions are true, the test will evaluate as passed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "connection.connection_test.passed"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some extra parameters are checked, and stored within the `data` (which stores useful info), and `extra` (which stores errors and minor details)\n",
    "\n",
    "The test also runs some basic timing checks and calculates a very rough latency:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.00192106564839681\n",
      "0.00192106564839681\n"
     ]
    }
   ],
   "source": [
    "# NBVAL_IGNORE_OUTPUT\n",
    "print(connection.connection_test.latency)\n",
    "\n",
    "print(connection.latency)  # this is also available from the root URL object"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    ".. versionadded:: 0.5.9\n",
    "\n",
    "Finally, for checking connections, `URL` provides a `ping()` method, which will attempt to run the `ping` command on your system, targeting the remote. This takes the arguments `n`, waiting for `n` returns from the remote, and `timeout`, which limits the total duration.\n",
    "\n",
    "### URL.cmd\n",
    "\n",
    "URL is a powerful interface between python and your remote system, the method that will likely be most used in your workflows is `URL.cmd()`, so we will cover its more advanced features here.\n",
    "\n",
    "Just as before, passing a string to the method will cause it to attempt to execute this in a bash shell on the remote:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "this command is executed on the remote"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "connection.cmd('echo \"this command is executed on the remote\"')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Error handling\n",
    "\n",
    "By default, cmd will raise any errors encountered. If cmd detects anything on `stderr` (that isn't an empty string), it will be raised as a RuntimeError:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "ename": "RuntimeError",
     "evalue": "received the following stderr: \n/bin/bash: -c: line 0: syntax error near unexpected token `do'\n/bin/bash: -c: line 0: `do a thing'\n",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mRuntimeError\u001b[0m                              Traceback (most recent call last)",
      "Cell \u001b[0;32mIn [5], line 3\u001b[0m\n\u001b[1;32m      1\u001b[0m \u001b[38;5;66;03m# NBVAL_RAISES_EXCEPTION\u001b[39;00m\n\u001b[1;32m      2\u001b[0m \u001b[38;5;66;03m# NBVAL_IGNORE_OUTPUT\u001b[39;00m\n\u001b[0;32m----> 3\u001b[0m \u001b[43mconnection\u001b[49m\u001b[38;5;241;43m.\u001b[39;49m\u001b[43mcmd\u001b[49m\u001b[43m(\u001b[49m\u001b[38;5;124;43m'\u001b[39;49m\u001b[38;5;124;43mdo a thing\u001b[39;49m\u001b[38;5;124;43m'\u001b[39;49m\u001b[43m)\u001b[49m\n",
      "File \u001b[0;32m~/remotemanager/remotemanager/connection/url.py:337\u001b[0m, in \u001b[0;36mURL.cmd\u001b[0;34m(self, cmd, asynchronous, local, stdout, stderr, timeout, max_timeouts, raise_errors, dry_run)\u001b[0m\n\u001b[1;32m    328\u001b[0m     \u001b[38;5;28;01mreturn\u001b[39;00m cmd\u001b[38;5;241m.\u001b[39mstrip()\n\u001b[1;32m    330\u001b[0m thiscmd \u001b[38;5;241m=\u001b[39m CMD(cmd\u001b[38;5;241m.\u001b[39mstrip(),\n\u001b[1;32m    331\u001b[0m               asynchronous\u001b[38;5;241m=\u001b[39masynchronous,\n\u001b[1;32m    332\u001b[0m               stdout\u001b[38;5;241m=\u001b[39mstdout,\n\u001b[0;32m   (...)\u001b[0m\n\u001b[1;32m    335\u001b[0m               max_timeouts\u001b[38;5;241m=\u001b[39mmax_timeouts,\n\u001b[1;32m    336\u001b[0m               raise_errors\u001b[38;5;241m=\u001b[39mraise_errors)\n\u001b[0;32m--> 337\u001b[0m \u001b[43mthiscmd\u001b[49m\u001b[38;5;241;43m.\u001b[39;49m\u001b[43mexec\u001b[49m\u001b[43m(\u001b[49m\u001b[43m)\u001b[49m\n\u001b[1;32m    338\u001b[0m \u001b[38;5;28;01mif\u001b[39;00m \u001b[38;5;129;01mnot\u001b[39;00m local:\n\u001b[1;32m    339\u001b[0m     \u001b[38;5;28mself\u001b[39m\u001b[38;5;241m.\u001b[39m_callcount \u001b[38;5;241m+\u001b[39m\u001b[38;5;241m=\u001b[39m \u001b[38;5;241m1\u001b[39m\n",
      "File \u001b[0;32m~/remotemanager/remotemanager/connection/cmd.py:254\u001b[0m, in \u001b[0;36mCMD.exec\u001b[0;34m(self)\u001b[0m\n\u001b[1;32m    251\u001b[0m     \u001b[38;5;28;01mreturn\u001b[39;00m \u001b[38;5;28mself\u001b[39m\u001b[38;5;241m.\u001b[39m_fexec(stdout, stderr)\n\u001b[1;32m    253\u001b[0m \u001b[38;5;28;01mtry\u001b[39;00m:\n\u001b[0;32m--> 254\u001b[0m     \u001b[38;5;28;43mself\u001b[39;49m\u001b[38;5;241;43m.\u001b[39;49m\u001b[43m_exec\u001b[49m\u001b[43m(\u001b[49m\u001b[43mstdout\u001b[49m\u001b[43m,\u001b[49m\u001b[43m \u001b[49m\u001b[43mstderr\u001b[49m\u001b[43m)\u001b[49m\n\u001b[1;32m    255\u001b[0m \u001b[38;5;28;01mexcept\u001b[39;00m \u001b[38;5;167;01mOSError\u001b[39;00m \u001b[38;5;28;01mas\u001b[39;00m E:\n\u001b[1;32m    256\u001b[0m     msg \u001b[38;5;241m=\u001b[39m \u001b[38;5;124m'\u001b[39m\u001b[38;5;124mEncountered an OSError on exec, attempting file exec\u001b[39m\u001b[38;5;124m'\u001b[39m\n",
      "File \u001b[0;32m~/remotemanager/remotemanager/connection/cmd.py:293\u001b[0m, in \u001b[0;36mCMD._exec\u001b[0;34m(self, stdout, stderr)\u001b[0m\n\u001b[1;32m    291\u001b[0m \u001b[38;5;28;01mif\u001b[39;00m \u001b[38;5;129;01mnot\u001b[39;00m \u001b[38;5;28mself\u001b[39m\u001b[38;5;241m.\u001b[39m_async \u001b[38;5;129;01mand\u001b[39;00m \u001b[38;5;129;01mnot\u001b[39;00m \u001b[38;5;28mself\u001b[39m\u001b[38;5;241m.\u001b[39mis_redirected:\n\u001b[1;32m    292\u001b[0m     \u001b[38;5;28mself\u001b[39m\u001b[38;5;241m.\u001b[39m_logger\u001b[38;5;241m.\u001b[39mdebug(\u001b[38;5;124m'\u001b[39m\u001b[38;5;124min-exec communication triggered\u001b[39m\u001b[38;5;124m'\u001b[39m)\n\u001b[0;32m--> 293\u001b[0m     \u001b[38;5;28;43mself\u001b[39;49m\u001b[38;5;241;43m.\u001b[39;49m\u001b[43mcommunicate\u001b[49m\u001b[43m(\u001b[49m\u001b[43m)\u001b[49m\n",
      "File \u001b[0;32m~/remotemanager/remotemanager/connection/cmd.py:391\u001b[0m, in \u001b[0;36mCMD.communicate\u001b[0;34m(self, use_cache, ignore_errors)\u001b[0m\n\u001b[1;32m    385\u001b[0m         \u001b[38;5;28;01mpass\u001b[39;00m\n\u001b[1;32m    387\u001b[0m \u001b[38;5;28;01mif\u001b[39;00m \u001b[38;5;28mself\u001b[39m\u001b[38;5;241m.\u001b[39m_raise_errors \\\n\u001b[1;32m    388\u001b[0m         \u001b[38;5;129;01mand\u001b[39;00m \u001b[38;5;129;01mnot\u001b[39;00m ignore_errors \\\n\u001b[1;32m    389\u001b[0m         \u001b[38;5;129;01mand\u001b[39;00m err \u001b[38;5;129;01mis\u001b[39;00m \u001b[38;5;129;01mnot\u001b[39;00m \u001b[38;5;28;01mNone\u001b[39;00m \\\n\u001b[1;32m    390\u001b[0m         \u001b[38;5;129;01mand\u001b[39;00m err \u001b[38;5;241m!=\u001b[39m \u001b[38;5;124m'\u001b[39m\u001b[38;5;124m'\u001b[39m:\n\u001b[0;32m--> 391\u001b[0m     \u001b[38;5;28;01mraise\u001b[39;00m \u001b[38;5;167;01mRuntimeError\u001b[39;00m(\u001b[38;5;124mf\u001b[39m\u001b[38;5;124m'\u001b[39m\u001b[38;5;124mreceived the following stderr: \u001b[39m\u001b[38;5;130;01m\\n\u001b[39;00m\u001b[38;5;132;01m{\u001b[39;00merr\u001b[38;5;132;01m}\u001b[39;00m\u001b[38;5;124m'\u001b[39m)\n\u001b[1;32m    393\u001b[0m \u001b[38;5;28;01mreturn\u001b[39;00m {\u001b[38;5;124m'\u001b[39m\u001b[38;5;124mstdout\u001b[39m\u001b[38;5;124m'\u001b[39m: _clean_output(std),\n\u001b[1;32m    394\u001b[0m         \u001b[38;5;124m'\u001b[39m\u001b[38;5;124mstderr\u001b[39m\u001b[38;5;124m'\u001b[39m: _clean_output(err)}\n",
      "\u001b[0;31mRuntimeError\u001b[0m: received the following stderr: \n/bin/bash: -c: line 0: syntax error near unexpected token `do'\n/bin/bash: -c: line 0: `do a thing'\n"
     ]
    }
   ],
   "source": [
    "# NBVAL_RAISES_EXCEPTION\n",
    "# NBVAL_IGNORE_OUTPUT\n",
    "connection.cmd('do a thing')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If this is undesirable for your workflow, you can pass the optional argument `raise_errors = False` \n",
    "\n",
    ".. note::\n",
    "    If you have a situation where non-fatal errors end up on stderr, `raise_errors=False` can be passed to the actual `URL`, which sets and `cmd` call to ignore errors by default.\n",
    "\n",
    "In addition to this, `cmd` also returns a `CMD` *object*, which can be queried after the fact\n",
    "\n",
    "The most useful properties are `stdout` and `stderr` which allow storage and querying of these attributes after a call.\n",
    "\n",
    ".. warning:: If the cmd call is made alone as in the previous cell, the `CMD` object is discarded. If you wish to further process the call, you must place it within a variable as below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "output = connection.cmd('do a thing', raise_errors=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('cmd stdout:', output.stdout)\n",
    "print('cmd stderr:', output.stderr)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are other useful attributes attached to this object, which may assist in your workflows, or debugging:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# NBVAL_IGNORE_OUTPUT\n",
    "print('Shell process id is:', output.pid)\n",
    "print('Working dir of the call is:', output.pwd)\n",
    "print('The command that was sent is:', output.sent)\n",
    "print('User that executed the cmd:', output.whoami)\n",
    "print('You also have access to the returned code:', output.returncode)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is also a `output.kill()` method, which will attempt to terminate the process"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Async calls\n",
    "\n",
    "Up until now we have been calling commands sequentially and waiting for the result, however it's possible to launch a command and proceed without waiting for this.\n",
    "\n",
    "Below we have 2 structures that issue a command that waits for 3s, then returns the string \"finished!\"\n",
    "\n",
    "We will time how long the execution takes, and how long it takes to get back the result:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import time\n",
    "\n",
    "t0 = time.time()\n",
    "output1 = connection.cmd('sleep 3 && echo \"finished!\"')\n",
    "\n",
    "t1 = time.time()\n",
    "dt = int(round(t1 - t0))\n",
    "print(f'call took ~{dt}s')\n",
    "\n",
    "print(output1)\n",
    "t2 = time.time()\n",
    "\n",
    "dt = int(round(t2 - t1))\n",
    "print(f'collecting the results took ~{dt}s')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t0 = time.time()\n",
    "output2 = connection.cmd('sleep 3 && echo \"finished!\"', asynchronous=True)\n",
    "\n",
    "t1 = time.time()\n",
    "dt = int(round(t1 - t0))\n",
    "print(f'call took ~{dt}s')\n",
    "\n",
    "print(output2)\n",
    "t2 = time.time()\n",
    "\n",
    "dt = int(round(t2 - t1))\n",
    "print(f'collecting the results took ~{dt}s')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can see, the first call waits for completion, returning the result. The second call however skips this waiting phase, and we don't actually have to wait for the command to execute until *after* we request the result"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Advanced Remote Techniques\n",
    "\n",
    "URL has some further options available which may enhance your workflows in a remote setting. To show these more in depth systems, we shall create a \"dummy\" connection:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dummy = URL(user='username', host='remote.connection.address')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The cmd call also has a few options, one which we shall be using for the dummy system is the `dry_run` flag, which can be useful if you want to store the command somewhere or just see what a call will do:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dummy.cmd('echo \"this call will just be returned as a string\"', dry_run=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An additionally useful flag which you may need to use a lot, is the `local`. This allows you to force a local run, even if the cmd is pointed at the remote. See the change in command here:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dummy.cmd('echo \"this call will just be returned as a string\"', local=True, dry_run=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Editing the ssh string\n",
    "\n",
    "URL has an `ssh` property which will return the string that allows interfacing with the remote. If this needs updating for whatever reason it can be overridden by simply setting the attribute. The example below is used to remove a perl language error that can occur on some systems."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('the initial ssh string is', dummy.ssh)\n",
    "\n",
    "dummy.ssh = 'LANG=C ' + dummy.ssh\n",
    "\n",
    "print('the updated ssh string is', dummy.ssh)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To undo this change, set `ssh` to `None`, or call `url.clear_ssh_override()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dummy.ssh = None\n",
    "dummy.clear_ssh_override()\n",
    "print('the reverted ssh string is', dummy.ssh)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### URL.utils\n",
    "\n",
    "URL also provides a `utils` module which provides both commonly used functions, and more complex ones. First of these is the `mkdir` and `touch` methods, which will create a dir and file with the given path, respectively"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_mtime = int(time.time())\n",
    "\n",
    "connection.utils.mkdir('temp_utils_test')\n",
    "connection.utils.touch('temp_utils_test/create_me')\n",
    "connection.utils.touch('temp_utils_test/create_me_also')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is also `utils.ls`, which returns the files as a list by default"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "connection.utils.ls('temp_utils_test')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The more powerful functions granted by utils is the `search_folder`, `file_presence` and `file_mtime` methods.\n",
    "\n",
    "These methods allow searching for a list of files, condensing the the query down to a single call in each case. This is useful for high latency remote systems, where an `ls` search for 100+ files could take a long time. These functions will do this in a single call.\n",
    "\n",
    "`search_folder` takes a list of files and a folder, returning a {file: bool} \"truth-dict\" of whether those files are present"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "connection.utils.search_folder(['create_me', 'not_present'], 'temp_utils_test')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly, a more general form exists in `file_presence`, which will take a list of files and return a similar truth-dict of their `ls` presence"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "connection.utils.file_presence(['temp_utils_test/create_me',\n",
    "                                'missing_folder/file',\n",
    "                                'temp_utils_test/not_present'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the file modification time is what you want, then `file_mtime` can be used in a similar way. This is the method called internally in `file_presence`, so incurrs no extra runtime"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# NBVAL_IGNORE_OUTPUT\n",
    "\n",
    "times = connection.utils.file_mtime(['temp_utils_test/create_me',\n",
    "                                     'temp_utils_test/create_me_also',\n",
    "                                     'temp_utils_test/not_present'])\n",
    "\n",
    "print(times)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "nbsphinx": "hidden"
   },
   "outputs": [],
   "source": [
    "for file, time in times.items():\n",
    "    if time is not None:\n",
    "        assert time == test_mtime, f'time {time} != {test_mtime}'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### ProxyJump\n",
    "\n",
    "If you can connect with an `ssh ...` command, you can do it using URL. An extension to this is `ProxyJump`, which allows you to \"hop\" between hosts to get to your destination. If you have this set up, you will have an ssh config file that looks somewhat like this:\n",
    "\n",
    "```\n",
    "    Host remote-endpoint\n",
    "        User username\n",
    "        Hostname remote.endpoint.address\n",
    "        ProxyJump remote-middleman\n",
    "    Host remote-middleman\n",
    "        User username\n",
    "        Hostname remote.middleman.address\n",
    "```\n",
    "        \n",
    "The following URL is an example that would connect using these parameters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "proxyurl = URL(host='remote-endpoint')\n",
    "\n",
    "print(proxyurl.userhost)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(proxyurl.cmd('echo \"test\"', dry_run=True))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
