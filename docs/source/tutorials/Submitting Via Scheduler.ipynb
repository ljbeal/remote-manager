{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "21ef027d-8785-4f71-bc5a-8b4adc96aec3",
   "metadata": {},
   "source": [
    "## Submitting jobs via scheduler\n",
    "\n",
    "Now you are able to connect to remote machines, it is time to discuss the methods for which you can submit jobs to a scheduler. There are two methods for this:\n",
    "\n",
    "- manually writing the jobscript and passing it to the dataset\n",
    "- creating a computer class for your machine\n",
    "\n",
    "For quick test jobs, it can be sufficient to simply write a jobscript specific to the test. We shall cover the basics with this.\n",
    "\n",
    "As in a usual run, we begin by defining our function, connection and dataset:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "2e100cef-a44b-47ec-ad79-f04c9ed6c71c",
   "metadata": {},
   "outputs": [],
   "source": [
    "from remotemanager import Dataset, URL\n",
    "\n",
    "def some_long_calculation(inp):\n",
    "    import time\n",
    "    \n",
    "    time.sleep(10)\n",
    "    \n",
    "    return inp**2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "ea051c46-d2c9-4c06-a269-d9c2384973cd",
   "metadata": {},
   "outputs": [],
   "source": [
    "# once again, for testing purposes, we shall use a \"local\" connection, however this can be any remote connection\n",
    "url = URL()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "47eb9d23-f2ce-4e7a-a089-b5371d3163c3",
   "metadata": {},
   "source": [
    "The dataset setup is somewhat different, however. You should specify the submitter required, and create your script header.\n",
    "\n",
    ".. note::\n",
    "    This example is for a vanilla SLURM scheduler system, be sure to customise this for your setup."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "55c94992-9b8e-4c57-9151-7073d2d62658",
   "metadata": {},
   "outputs": [],
   "source": [
    "script = \"\"\"\n",
    "#!/bin/bash\n",
    "#SBATCH --job-name=test_job \n",
    "#SBATCH --ntasks=16\n",
    "#SBATCH --time=00:30:00\n",
    "\n",
    "module load python\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "46f39c51-c13f-4117-86b0-a589dade7443",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = Dataset(function = some_long_calculation,\n",
    "             url = url,\n",
    "             local_dir = 'temp_scheduler_local',\n",
    "             remote_dir = 'temp_scheduler_remote',\n",
    "             script = script,\n",
    "             submitter = 'sbatch')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "073d200a-0e2b-4192-9509-f1732c8cfa6a",
   "metadata": {},
   "source": [
    "Adding a run and dry running will allow us to see what files were created:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "02065e0f-4c65-4197-951d-d426b9ff4412",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "appended run runner-0\n",
      "assessing run for runner dataset-8fc7680c-runner-0... checks passed, running\n",
      "launch command: cd temp_scheduler_remote && bash 0-dataset-365fd94f-master.sh\n"
     ]
    }
   ],
   "source": [
    "ds.append_run(args={'inp': 7})\n",
    "ds.run(dry_run=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fbbb3982-cb34-4e22-b509-515368cd1ae6",
   "metadata": {},
   "source": [
    "Executing the run here has created the necessary files on the local side, and has stopped prior to sending. So now we can see what was created, and this is a useful tool to explain how the run system works.\n",
    "\n",
    "In short, running is a 3 tier process, involving 3 files:\n",
    "\n",
    "+-------------------------+--------------------------------------------------+\n",
    "| File                    | Purpose                                          |\n",
    "+=========================+==================================================+\n",
    "| Dataset \"master\" script | Dataset level script that submits the jobscripts |\n",
    "+-------------------------+--------------------------------------------------+\n",
    "| Runner jobscript        | Runner level script that requests resources      |\n",
    "+-------------------------+--------------------------------------------------+\n",
    "| Runner python script    | Contains your function, returning the result     |\n",
    "+-------------------------+--------------------------------------------------+\n",
    "\n",
    "Lets start by printing the contents of the master script:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "e6e6d25a-7954-4c7b-9c7f-036c3da106ef",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "sbatch dataset-8fc7680c-runner-0-jobscript.sh\n"
     ]
    }
   ],
   "source": [
    "with open(f'temp_scheduler_local/0-{ds.master_script}', 'r') as o:\n",
    "    print(o.read())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3a5aefea-68f7-4463-995f-8f0416a9d009",
   "metadata": {},
   "source": [
    "Here we see that it contains a single line, for our runner, and uses our set `submitter` to submit it. There will be a line for each runner, using the set `submitter`, which defaults to `bash`.\n",
    "\n",
    "Next up is the actual jobscript for the runner:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "b4b51247-a6f4-428e-b201-56b0dff324cb",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "#!/bin/bash\n",
      "#SBATCH --job-name=test_job \n",
      "#SBATCH --ntasks=16\n",
      "#SBATCH --time=00:30:00\n",
      "\n",
      "module load python\n",
      "\n",
      "python dataset-8fc7680c-runner-0-run.py\n"
     ]
    }
   ],
   "source": [
    "with open(ds.runners[0].jobscript.local, 'r') as o:\n",
    "    print(o.read())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "65cd937c-adff-4113-9403-737155a2eafb",
   "metadata": {},
   "source": [
    "Note how the top section of this jobscript is simply the `script` we defined earlier. To the base of this, is appended our python submission line.\n",
    "\n",
    "#### Changing python\n",
    "\n",
    "But what if our remote machine has a different python call? This can be the case for a lot of machines, the most common alternative being `python3` being required for calling an up to date python version. As this property is a factor of the remote system, this is set via the `URL`. Lets define a new url with a different python call, and repeat the above steps:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "b85a6a97-2f18-4132-bd59-a969820917c5",
   "metadata": {},
   "outputs": [],
   "source": [
    "url = URL(python='python3')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "d25548ea-55aa-4f83-9f53-c04ae4c107cc",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "assessing run for runner dataset-8fc7680c-runner-0... checks passed, running\n",
      "launch command: cd temp_scheduler_remote && bash 0-dataset-365fd94f-master.sh\n",
      "\n",
      "#!/bin/bash\n",
      "#SBATCH --job-name=test_job \n",
      "#SBATCH --ntasks=16\n",
      "#SBATCH --time=00:30:00\n",
      "\n",
      "module load python\n",
      "\n",
      "python3 dataset-8fc7680c-runner-0-run.py\n"
     ]
    }
   ],
   "source": [
    "ds.url = url\n",
    "ds.run(dry_run=True)\n",
    "\n",
    "with open(ds.runners[0].jobscript.local, 'r') as o:\n",
    "    print(o.read())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "01690625-3ac7-4293-9b84-93756969c64b",
   "metadata": {},
   "source": [
    "#### Creating a Simple Computer\n",
    "\n",
    "Though more complex than providing a script as a string, the computer classes have been designed to be as easy as possible to create. In fact, `Dataset` only has two conditions when attempting to use a computer script:\n",
    "\n",
    "- The object is a URL or URL subclass\n",
    "- The object has a script() method which returns a string\n",
    "\n",
    ".. Note::\n",
    "    If not copying from `Example_Computer`, make sure to add the `super().__init__(**kwargs)` call to the `__init__` method of your computer class. Omitting this line can lead to strange behaviour\n",
    "\n",
    "If these conditions are fulfilled, Dataset will use whatever is returned from the `script()` method as the jobscript. Because of this, we can create a very basic computer like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "41bc3531-3dbb-4ffb-b1ac-90baac7acd55",
   "metadata": {},
   "outputs": [],
   "source": [
    "class computer(URL):\n",
    "    \n",
    "    def script(self, *args, **kwargs):\n",
    "        mpi = kwargs.get('mpi')  # we can get any submission parameters from the kwargs\n",
    "        return f\"Some jobscript string\\nThe run command will be placed below this\\nrunning with mpi={mpi}\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "1ebe7e4b-77ea-47a3-a610-327dd61090b8",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "appended run runner-0\n",
      "assessing run for runner dataset-aac2ac4a-runner-0... checks passed, running\n",
      "launch command: cd temp_scheduler_remote && bash 0-dataset-365fd94f-master.sh\n"
     ]
    }
   ],
   "source": [
    "comp = computer()\n",
    "\n",
    "ds = Dataset(function=some_long_calculation,\n",
    "             url=comp,\n",
    "             local_dir='temp_scheduler_local',\n",
    "             remote_dir='temp_scheduler_remote',\n",
    "             block_reinit=True,\n",
    "             mpi=16)\n",
    "\n",
    "ds.append_run(args={'inp': 1})\n",
    "ds.run(dry_run = True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4f25415d-7044-4257-9c86-a2c9cf6d0a44",
   "metadata": {},
   "source": [
    "Lets see what the job scripts look like"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "3b723b6f-9440-4ef4-9f32-c63c66151357",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Some jobscript string\n",
      "The run command will be placed below this\n",
      "running with mpi=16\n",
      "python dataset-aac2ac4a-runner-0-run.py\n"
     ]
    }
   ],
   "source": [
    "with open(ds.runners[0].jobscript.local, 'r') as o:\n",
    "    print(o.read())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "78a971de-2f8c-47c9-888c-92d462d6260a",
   "metadata": {},
   "source": [
    "#### Writing your own Computer class\n",
    "\n",
    "This approach is more in depth, but absolutely saves time in the long run if you're repeatedly using a machine. Once you have a base class for your machine, it can be tweaked as you see fit. It even adapted to similar machines.\n",
    "\n",
    "There are two example classes in the `connection.computers.example` module, which may help you in this approach, however the basic principle is to subclass `BaseComputer`, adding the required parameters. This should take the arguments required for running jobs on your machine.\n",
    "\n",
    "See [computers.example](../remotemanager.connection.computers.example.html) for the docs of this file, or continue here for a basic explanation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "47dad02a-af81-414a-94d3-d9658d1fdf1b",
   "metadata": {},
   "outputs": [],
   "source": [
    "from remotemanager.connection.computers.example import Example_Torque, Example_Slurm"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "15f3f1e6-3326-48c9-8424-5e96e15df9f3",
   "metadata": {},
   "outputs": [],
   "source": [
    "url = Example_Slurm()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "248cf20a-7c7c-4d7e-9936-e31caf00372c",
   "metadata": {},
   "source": [
    "As this computer object is a subclass of `URL`, it contains everything you would expect from it, with some small changes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "212d10b7-96f0-4a41-a5d9-d5c327096680",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "host address is set as remote.address.for.connection\n",
      "it comes pre-defined without a user. User is: None\n"
     ]
    }
   ],
   "source": [
    "print('host address is set as', url.host)\n",
    "print('it comes pre-defined without a user. User is:', url.user)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a2c959bb-2f4e-4430-877f-0c3906bc21d8",
   "metadata": {},
   "source": [
    "You can leave your username blank for later updating, or set at instantisation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "8e1931da-27ae-4afb-a398-c36e9619f6cd",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "username@remote.address.for.connection\n"
     ]
    }
   ],
   "source": [
    "url = Example_Slurm(user='username')\n",
    "\n",
    "print(url.userhost)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "72610d61-43bc-4713-8bf9-72ce3df8c4b4",
   "metadata": {},
   "source": [
    "You can also override basic url methods and attributes. For example, the SSH command has the change that was used in the URL tutorial:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "84899c65-3874-45a7-8263-43899d3d8483",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "ssh -p 22 username@remote.address.for.connection\n"
     ]
    }
   ],
   "source": [
    "print(url.ssh)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "be1d0528-834f-4a90-9d69-1a58160d4494",
   "metadata": {},
   "source": [
    "This is done by adding the `ssh` property to the subclass, as it will be called in priority over the base `URL` version.\n",
    "\n",
    "The greatest example of this method, however, is scripts can be on-demand formatted with arguments. The example script has a Torque scheduler base config, and is used internally like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "38ffb963-8b61-4d96-88a7-9225ff58e73b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "#!/bin/bash\n",
      "#SBATCH --cpus-per-task=4\n",
      "#SBATCH --job-name=test_job\n",
      "#SBATCH --nodes=2\n",
      "#SBATCH --ntasks=4\n",
      "#SBATCH --queue=test_queue\n",
      "#SBATCH --walltime=00:30:00\n",
      "\n",
      "module purge\n",
      "module load python3\n",
      "\n"
     ]
    }
   ],
   "source": [
    "print(url.script(mpi=4, omp=4, nodes=2, name='test_job', walltime='00:30:00', queue='test_queue'))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6c3c80ed-7391-47b9-bf6e-8ab37d2a16c7",
   "metadata": {},
   "source": [
    "This would allow for jobs to be submitted by configuring these arguments, loading the required modules and setting environment variables as needed."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42e40113-def5-499a-bad7-25ca83593779",
   "metadata": {
    "tags": []
   },
   "source": [
    "You can proceed to make this class as complex as you wish, though you may benefit by moving onwards to creating a class using the `BaseComputer` class (next)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
