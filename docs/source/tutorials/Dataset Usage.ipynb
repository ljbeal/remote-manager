{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dataset Usage\n",
    "\n",
    "`Dataset` is the primary class of the package. It is a general purpose \"container\" which will store your function to be serialised, and allow various other processess to be applied. The most useful of which are running, and transferring."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the purposes of this tutorial, we will bring back our basic `multiply` function you may have seen in the quickstart guide. Though we will amend it such that the delay time is adjustable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "def multiply(a, b, t=1):\n",
    "    import time\n",
    "    \n",
    "    time.sleep(t)\n",
    "    \n",
    "    return a * b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now set up a `Dataset`. The only _required_ argument is the function, though there are many other optional arguments, most of which we shall also cover in this tutorial. See the [Dataset API documentation](../remotemanager.dataset.dataset.html) for full details.\n",
    "\n",
    "Again for this tutorial we will be using a local url, this enables the functions to run anywhere and be tested. \n",
    "\n",
    ".. note:: \n",
    "    You should take the concept that the url simply dictates _where_ the dataset will be run, so should \n",
    "    you have access to a remote machine, this notebook would run identically if you were to swap out \n",
    "    the URL for one which connected with that machine."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "import remotemanager\n",
    "import time\n",
    "\n",
    "remotemanager.Logger.path = 'Dataset_Tutorial'\n",
    "remotemanager.Logger.level = 'debug'\n",
    "\n",
    "from remotemanager import Dataset, URL\n",
    "\n",
    "url = URL('localhost')\n",
    "\n",
    "ds = Dataset(function=multiply,\n",
    "             url=url,\n",
    "             script='#!/bin/bash',\n",
    "             submitter='bash',\n",
    "             local_dir='temp_ds_staging',\n",
    "             remote_dir='temp_ds_remote',\n",
    "             dbfile='temp_database_file')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The arguments shown here are likely to be the ones used the most. So in short:\n",
    "\n",
    "\n",
    "- `url`: The remote connection, and it is very rare that you would need to omit it\n",
    "- `script`: For use when submitting to schedulers, create your jobscript as a string and pass it here\n",
    "- `submitter`: Also for use when using a scheduler, this command will be used to launch your job\n",
    "- `local_dir`: This is the directory that will be used to \"stage\" your files before sending to the remote. Defaults to `temp_runner_local`\n",
    "- `remote_dir`: Remote directory where files will be sent to. Defaults to `temp_runner_remote`\n",
    "- `dbfile`: Here you can force the dataset to store its database within a specific filename, should you wish to keep track of this. Otherwise, it defaults to `{self.name}-{self.short_uuid}.yaml`.\n",
    "\n",
    ".. note::\n",
    "    `remote_dir` defaults to `run_dir`, or `runner_remote` if not present. If _both_ `remote_dir` and `run_dir` are present, a new `run_dir` folder will be created within `remote_dir` for the excecution of the script. This is helpful if your function outputs files that you would like to compartmentalise.\n",
    "\n",
    "If you took a peek at the API documentation you may have noticed that the arguments `local_dir` and `run_dir` are not present in the `Dataset` initialisation. This is because any arguments specified in this block are just for the `Dataset` itself, and the remaining args are passed through to `run_args`. These are then provided to the runners as default arguments when appending runs. See the [Runner API Documentation](../remotemanager.dataset.runner.html) for more info on this.\n",
    "\n",
    "For example, in this situation we would want all runs to run within the `remote_run_dir` directory, however lets imagine that we have _one_ run which we want to run in `remote_run_alone` (or some other specific dir): \n",
    "\n",
    "`Dataset.append_run()` also has a `run_args` catch, so when appending this run, if `run_dir='remote_run_alone'` is passed, it will override this default and the runner will run where you requested. This goes for all arguments that the runners can take.\n",
    "\n",
    "The current run args can be seen via the `Dataset.global_run_args` property. \n",
    "\n",
    "Side note: Currently, any run args added after Dataset initialisation (for example, if you were to call `ds.mpi = 16`) will be \"faked\" by this attribute. This is done by an introspection of the properties available, adding any that don't exist in the base `Dataset` object, ignoring any `_private` variables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'skip': True, 'local_dir': 'temp_ds_staging', 'remote_dir': 'temp_ds_remote'}"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ds.global_run_args"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Appending Runs\n",
    "\n",
    "Before running your function you must append runs containing any arguments.\n",
    "\n",
    ".. note::\n",
    "    This is also true for runs that take no arguments, simply call `append_run()`\n",
    "    \n",
    "`Dataset.append_run()` allows for run creation, and at minimum requires a `dict` containing the required arguments for your function.\n",
    "\n",
    "So in our case, a dictionary containing arguments for `a` and `b` are necessary for a run to begin. As `t` has a default value of 1, it is optional. The structure below will append 3 runs displaying this behaviour:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "appended run runner-0\n",
      "appended run runner-1\n",
      "appended run runner-2\n"
     ]
    }
   ],
   "source": [
    "runs = [{'a': 10, 'b': 5},\n",
    "        {'a': 5.7, 'b': 8.4},\n",
    "        {'a': 4, 'b': 4, 't': 6}]\n",
    "\n",
    "for run in runs:\n",
    "    ds.append_run(args=run)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    ".. note:: There is also the alias `arguments` for `args`\n",
    "\n",
    "Additonally, if you wish to run scripts within unique folders, you can specify a `run_dir` when appending runs. If this attribute is present, this folder will be created within the remote dir and the function will be run from within. You may need to adjust your scripts and additional files to suit this run behaviour."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The Runner object\n",
    "\n",
    "Now we have a dataset which is able to be run and return our results. Before we do this, it is worth stepping through some useful debugging tools.\n",
    "\n",
    "Firstly, how to query what runs you already have. This can be done by accessing the property `Dataset.runners`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[dataset-d37cdb62-runner-0,\n",
       " dataset-7e0c6a09-runner-1,\n",
       " dataset-b0e37812-runner-2]"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ds.runners"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is also the `runner_dict` property, which returns the same information in dict(append id: runner) format"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'runner-0': dataset-d37cdb62-runner-0,\n",
       " 'runner-1': dataset-7e0c6a09-runner-1,\n",
       " 'runner-2': dataset-b0e37812-runner-2}"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ds.runner_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see here the `Runner` object holding the arguments for our 3 runs. To see what's in these objects, access the list. \n",
    "\n",
    ".. note::\n",
    "    You should only ever really use this for debugging purposes, as most attributes of `Runner` exist for usage by the `Dataset` object itself"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'a': 10, 'b': 5}\n",
      "{'skip': True, 'local_dir': 'temp_ds_staging', 'remote_dir': 'temp_ds_remote'}\n"
     ]
    }
   ],
   "source": [
    "print(ds.runners[0].args)\n",
    "print(ds.runners[0].run_args)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Transferring Datasets\n",
    "\n",
    "remotemanager comes with a packaging system that allows you to send items across machines. To use this for your own purposes, you must subclass `SendableMixin` from `remotemanager.serialisation.sendablemixin`\n",
    "\n",
    "Though as this is a tutorial for Dataset, we will simply be using the methods that this class provides to Dataset.\n",
    "\n",
    "To package up a dataset, call the `pack()` method. This will return a dictionary that when dumped to yaml, will allow sending.\n",
    "\n",
    "`pack` also takes a `file` keyword, which will do this packaging for you:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "dumping payload to temp_payload.yaml\n"
     ]
    }
   ],
   "source": [
    "ds.pack(file='temp_payload.yaml')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "from this \"payload\" we can re-create the object wherever we need\n",
    "\n",
    ".. note::\n",
    "    As packaging is considered advanced usage, and as such, the `file` keyword _must_ be used.\n",
    "    \n",
    "As you can see below, recreating the object into `newds`, the runner information is identical to what we printed from `ds` earlier\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'a': 10, 'b': 5}\n",
      "{'local_dir': 'temp_ds_staging', 'remote_dir': 'temp_ds_remote', 'skip': True}\n"
     ]
    }
   ],
   "source": [
    "newds = Dataset.unpack(file='temp_payload.yaml')\n",
    "print(newds.runners[0].args)\n",
    "print(newds.runners[0].run_args)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    ".. Note::\n",
    "    it may be useful to you to have access to the uuid of your dataset, for debugging purposes. This can be accessed at `ds.uuid`, or `ds.short_uuid`. The latter is simply the first 8 characters, used for files. This also applies to any Runners"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "58dc25cb6bfac5262e21960eba5bd9f9188050e81e7c5bb291d2dee264e28e77\n",
      "58dc25cb\n"
     ]
    }
   ],
   "source": [
    "print(newds.uuid)\n",
    "print(newds.short_uuid)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Running the Datasets\n",
    "\n",
    "Running of the datasets is done via the `Dataset.run()` method. This gives you one final opportunity to override any run arguments, as it provides another `run_args` catch for extra keyword args.\n",
    "\n",
    ".. note::\n",
    "    Be aware of the argument expansion limitation that exists with rsync versions below 3. If you get errors during transfer, be sure to check `rsync --version` >= 3."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "assessing run for runner dataset-d37cdb62-runner-0... checks passed, running\n",
      "assessing run for runner dataset-7e0c6a09-runner-1... checks passed, running\n",
      "assessing run for runner dataset-b0e37812-runner-2... checks passed, running\n"
     ]
    }
   ],
   "source": [
    "newds.run()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you're following along on your machine you may have noticed that this call completed instantly, yet our function has a `time.sleep` line in it. We should have to wait 8s for this (1+1+6s delays).\n",
    "\n",
    "This is because the dataset run defaults to be asynchronous, and as you can imagine, this can be updated by passing this as a `run_arg` wherever you wish.\n",
    "\n",
    "Additionally here, we must use the `force=True` keyword to ensure that the runs go through, as the previous runs are still running. Otherwise, we could wait for them to complete. Be careful using this keyword in your workflows with long jobs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "assessing run for runner dataset-d37cdb62-runner-0... force running\n",
      "assessing run for runner dataset-7e0c6a09-runner-1... force running\n",
      "assessing run for runner dataset-b0e37812-runner-2... force running\n",
      "run completed in ~8s\n"
     ]
    }
   ],
   "source": [
    "# this line can be ignored in general use, currently omitting it can cause the CI to behave strangely\n",
    "url.cmd(f'rm -r {newds.remote_dir}')\n",
    "\n",
    "import time\n",
    "t0 = time.time()\n",
    "\n",
    "newds.run(asynchronous=False,\n",
    "          force=True)\n",
    "\n",
    "dt = int(time.time() - t0)\n",
    "print(f'run completed in ~{dt}s')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While not particularly useful in a wide range of use cases, there may be a situation case where you want to _wait_ for a short run to complete, and this also displays the amending of run variables nicely.\n",
    "\n",
    "One final way you are able to set the run args is via the `set_run_option` method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n",
      "value!\n"
     ]
    }
   ],
   "source": [
    "newds.set_run_option('asynchronous', True)\n",
    "print(newds.asynchronous)\n",
    "\n",
    "newds.set_run_option('new_option', 'value!')\n",
    "print(newds.new_option)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Post run functions\n",
    "\n",
    "There are functions indended to be used after a run has been called, to interact with the run, or the results.\n",
    "\n",
    "We shall cover:\n",
    "\n",
    "- is_finished\n",
    "- all_finished\n",
    "- fetch_results\n",
    "- results\n",
    "\n",
    "### `Dataset.is_finished`\n",
    "\n",
    "This property will return a boolean list of the `is_finished` method of the runners\n",
    "\n",
    "### `Dataset.all_finished`\n",
    "\n",
    "This property returns the all() of `Dataset.is_finished`\n",
    "\n",
    "To demonstrate these, we shall re-run and see what the state looks like at a few time intervals. But first, we must make sure that the results are not already present."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "wiping result files and relaunching...\n",
      "assessing run for runner dataset-d37cdb62-runner-0... checks passed, running\n",
      "assessing run for runner dataset-7e0c6a09-runner-1... checks passed, running\n",
      "assessing run for runner dataset-b0e37812-runner-2... checks passed, running\n",
      "calcs launched, waiting before checking completion\n",
      "\n",
      "after 2s, state is now:\n",
      "checking remotely for finished runs\n",
      "[True, True, False]\n",
      "checking remotely for finished runs\n",
      "all_finished: False\n",
      "\n",
      "after 7s, state is now:\n",
      "checking remotely for finished runs\n",
      "[True, True, True]\n",
      "checking remotely for finished runs\n",
      "all_finished: True\n"
     ]
    }
   ],
   "source": [
    "print('wiping result files and relaunching...')\n",
    "\n",
    "# this function will clear any runner results, and attempt to delete their local files\n",
    "newds.clear_results()\n",
    "newds.run(asynchronous=True)\n",
    "print('calcs launched, waiting before checking completion')\n",
    "\n",
    "time.sleep(2)\n",
    "\n",
    "print('\\nafter 2s, state is now:')\n",
    "print(newds.is_finished)\n",
    "print('all_finished:', newds.all_finished)\n",
    "\n",
    "time.sleep(5)\n",
    "\n",
    "print('\\nafter 7s, state is now:')\n",
    "print(newds.is_finished)\n",
    "print('all_finished:', newds.all_finished)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It may seem counter-intuitive that the runs are all completed at 7s, but if we recall that they were launched asynchronously, the whole run would take around 6s (our maximum delay time).\n",
    "\n",
    "#### The remaining functions\n",
    "\n",
    "#### `Dataset.fetch_results()`\n",
    "\n",
    "This function will attempt to grab any results from files or function objects that are attached to the dataset, storing them in the `results` property\n",
    "\n",
    "#### `Dataset.results`\n",
    "\n",
    "This property allows optimised access to the results of the previous run"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "checking remotely for finished runs\n"
     ]
    }
   ],
   "source": [
    "newds.fetch_results()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[50, 47.88, 16]"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "newds.results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Further features\n",
    "\n",
    "While we touched on the runner availability earlier, we skipped over a feature which may be helpful for debugging purposes. The `Runner` object has a `history` property which prints a {time: state} dict that contains information about all state changes the runner has experienced"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'2022-12-20 13:13:56/0': 'created',\n",
       " '2022-12-20 13:13:57/0': 'creating files and submitting...',\n",
       " '2022-12-20 13:13:57/1': 'runfile written',\n",
       " '2022-12-20 13:13:57/2': 'jobscript written',\n",
       " '2022-12-20 13:13:57/3': 'files copied to remote',\n",
       " '2022-12-20 13:13:57/4': 'command executed remotely',\n",
       " '2022-12-20 13:13:57/5': 'creating files and submitting...',\n",
       " '2022-12-20 13:13:57/6': 'runfile written',\n",
       " '2022-12-20 13:13:57/7': 'jobscript written',\n",
       " '2022-12-20 13:13:57/8': 'files copied to remote',\n",
       " '2022-12-20 13:14:05/0': 'command executed remotely',\n",
       " '2022-12-20 13:14:05/1': 'result wiped',\n",
       " '2022-12-20 13:14:05/2': 'creating files and submitting...',\n",
       " '2022-12-20 13:14:05/3': 'runfile written',\n",
       " '2022-12-20 13:14:05/4': 'jobscript written',\n",
       " '2022-12-20 13:14:05/5': 'files copied to remote',\n",
       " '2022-12-20 13:14:05/6': 'command executed remotely',\n",
       " '2022-12-20 13:14:06/0': 'resultfile created remotely',\n",
       " '2022-12-20 13:14:12/0': 'completed'}"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# NBVAL_IGNORE_OUTPUT\n",
    "newds.runners[0].history"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "here you can see the state history for the first runner in the list, showing the three runs, the creation time of the resultfile on the remote, and the final completion state where the results were loaded back into the runner\n",
    "\n",
    "If you just require a list of states (for example, checking if a runner has passed through a state), there is the property `Runner.status_list`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['created',\n",
       " 'creating files and submitting...',\n",
       " 'runfile written',\n",
       " 'jobscript written',\n",
       " 'files copied to remote',\n",
       " 'command executed remotely',\n",
       " 'creating files and submitting...',\n",
       " 'runfile written',\n",
       " 'jobscript written',\n",
       " 'files copied to remote',\n",
       " 'command executed remotely',\n",
       " 'result wiped',\n",
       " 'creating files and submitting...',\n",
       " 'runfile written',\n",
       " 'jobscript written',\n",
       " 'files copied to remote',\n",
       " 'command executed remotely',\n",
       " 'resultfile created remotely',\n",
       " 'completed']"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "newds.runners[0].status_list"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also query all runners and check for a set state. This is done using `get_all_runner_states` and `check_all_runner_states`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['completed', 'completed', 'completed']"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "newds.get_all_runner_states()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "newds.check_all_runner_states('completed')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Swapping out the serialiser\n",
    "\n",
    "By default, anything sent over ssh will be serialised using YAML format. Should you wish to use another format, there are currently 3 inbuilt serialisers within `remotemanager.serialisation` you can swap to:\n",
    "\n",
    "- `serialyaml`\n",
    "- `serialjson`\n",
    "- `serialdill`\n",
    "\n",
    "to swap these out, simply import them and pass them at dataset creation, as you would a URL:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [],
   "source": [
    "from remotemanager.serialisation import serialdill\n",
    "\n",
    "ser = serialdill()\n",
    "\n",
    "dillds = Dataset(function=multiply,\n",
    "                 url=url,\n",
    "                 serialiser=ser,\n",
    "                 name='dill_test_run',\n",
    "                 local_dir='temp_ds_staging',\n",
    "                 run_dir='temp_remote_run_dir')\n",
    "\n",
    "# when we're running a lot of datasets together, adding a checksum flag to the transport makes sure that rsync behaves properly\n",
    "dillds.transport.flags += '--checksum'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "appended run runner-0\n",
      "assessing run for runner dill_test_run-4f477a0c-runner-0... checks passed, running\n",
      "checking remotely for finished runs\n",
      "[48]\n"
     ]
    }
   ],
   "source": [
    "dillds.append_run(args={'a': 8, 'b': 6})\n",
    "dillds.run()\n",
    "time.sleep(2)\n",
    "dillds.fetch_results()\n",
    "print(dillds.results)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This run will have produced a .dill file within the run dir:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "temp_remote_run_dir/dill_test_run-4f477a0c-runner-0-result.dill"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "url.cmd('ls temp_remote_run_dir/*.dill')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Access to the commands used to execute the runs\n",
    "\n",
    "Once you have run a dataset, you can access the commands used to execute the bash scripts. This can be useful for debugging purposes, but note that this is done for you on `fetch_results`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "raw command: 'cd temp_ds_remote && bash 0-dataset-58dc25cb-master.sh'\n",
      "returned stdout: \n",
      "returned stderr: \n"
     ]
    }
   ],
   "source": [
    "for cmd in newds.run_cmds:\n",
    "    print('raw command:', cmd.sent)\n",
    "    print('returned stdout:', cmd.stdout)\n",
    "    print('returned stderr:', cmd.stderr)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "#### Creating your own serialiser\n",
    "\n",
    "If none of these options suit your use case, there remains the possibility of creating your own class. To do so, you should subclass `../remotemanager.serialisation.serial` [docs link here](../remotemanager.serialisation.serial.html), implementing the methods as seen in the various subclasses of `serial` [yaml, for example](../remotemanager.serialisation.yaml.html)\n",
    "\n",
    "### Running a single runner\n",
    "\n",
    "While it was mentioned previously that the runners themselves should ideally not be touched, and all interaction should be done via the `Dataset`, it _is_ possible to run a single runner if necessary.\n",
    "\n",
    ".. warning::\n",
    "    this process is inefficient and should only be used if absolutely required. It may be preferable to clear the results of the offending runner using `clear_results()` and rerunning with `skip=True`\n",
    "    \n",
    "We can again here demonstrate the use of `check_all_runner_states`, as we have only run one, checking for full completion will return False. Obviously in this case, `all_finished` will do the job, but you can query here for any state, such as `submitted`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "assessing run for runner dataset-d37cdb62-runner-0... checks passed, running\n",
      "checking remotely for finished runs\n",
      "[50, None, None]\n",
      "False\n"
     ]
    }
   ],
   "source": [
    "newds.clear_results()  # clear results to demonstrate\n",
    "\n",
    "newds.runners[0].run(asynchronous=False)\n",
    "time.sleep(1)\n",
    "newds.fetch_results()\n",
    "time.sleep(1)\n",
    "print(newds.results)\n",
    "\n",
    "print(newds.check_all_runner_states('completed'))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
