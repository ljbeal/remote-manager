{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Quickstart\n",
    "\n",
    "This notebook will cover all the basic and most useful functionality available to get a user up and running as fast as possible.\n",
    "\n",
    "We will skim over these topics:\n",
    "\n",
    "- installation\n",
    "- function definition\n",
    "- remote connection\n",
    "- dataset creation\n",
    "- creating runs\n",
    "- running and retrieving results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Installation\n",
    "\n",
    "Installation can be done via a pip install:\n",
    "\n",
    "`pip install remotemanager` for the most recent stable version.\n",
    "\n",
    "However if you would like the bleeding edge version, you can clone the `devel` branch of the git [repository](https://gitlab.com/ljbeal/remotemanager):\n",
    "\n",
    "`git clone --branch devel`\n",
    "\n",
    "`cd remotemanager && pip install .`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Function Definition\n",
    "\n",
    "remotemanager executes user defined python functions at the location of choice. Below is a basic function example which will serve our purposes for this guide\n",
    "\n",
    ".. note::\n",
    "    The function must stand by itself when running, so any imports or necessary functionality should be contained within"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "def multiply(a, b):\n",
    "    import time\n",
    "    \n",
    "    time.sleep(1)\n",
    "    \n",
    "    return a * b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Remote Connection\n",
    "\n",
    "This function would run just fine on any workstation, however imagine that the function is something significantly more demanding. We would need to connect to some more powerful resources for this.\n",
    "\n",
    "remotemanager provides the powerful URL module for this purpose"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "from remotemanager import URL\n",
    "\n",
    "connection = URL(host='localhost')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This example connection is simply pointed at `localhost`, however you may define a connection to a machine with address or IP:\n",
    "\n",
    "`connection = URL(user='username', host='remote.connection.address')`\n",
    "\n",
    "`connection = URL(user='username', host='192.168.123.456')`\n",
    "\n",
    ".. note::\n",
    "    The only requirement for `URL` to function is that you must be able to ssh into the remote machine without any additional prompts from the remote. For connection difficulties regarding permssions, see the [relevant section](../Introduction.html#Connecting-to-a-Remote-Machine) of the introduction.\n",
    "\n",
    "### Running Commands\n",
    "\n",
    "With the concept of this remote `connection`, we can excecute commands and (more importantly) our function on this machine.\n",
    "\n",
    "For commands, url provides a `cmd` method, which will execute any strings given"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "this command is executed on the remote"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "connection.cmd('echo \"this command is executed on the remote\"')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Running Functions\n",
    "\n",
    "For function execution, we require a `Dataset`. Think of this dataset as a container for your function, with calculations to be added later on.\n",
    "\n",
    "Like `URL`, this can be imported directly from `remotemanager`\n",
    "\n",
    "To create a dataset, the only requirement is a callable function object. You must pass this object to the Dataset\n",
    "\n",
    ".. note::\n",
    "    When passing a function to the dataset, do not call it within the assigment. For example, for our multiply function, we should pass `function=multiply` _not_ `function=multiply()`\n",
    "\n",
    "Here we are additionally specifying the `local_dir` and the `remote_dir`, which tells the Dataset where to put all relevant files on the local and remote machines, respectively.\n",
    "\n",
    "If it suits your workflow, you can additionally specify a `run_dir` when appending a run. This is an additional folder within `remote_dir` where the script will be executed from. Thus, any files created by your function will be placed here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "from remotemanager import Dataset\n",
    "\n",
    "ds = Dataset(function=multiply,\n",
    "             url=connection,\n",
    "             local_dir='temp_local',\n",
    "             remote_dir='temp_remote')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating runs\n",
    "\n",
    "As the dataset is simply a container for the function, it is essentially useless in this state. To get some use out of it, we must append some runs.\n",
    "\n",
    "To do this we use the `Dataset.append_run()` method. This will take the arguments in `dict` format, and store them for later.\n",
    "\n",
    "You may do this in any way you see fit, the important part is to pass a dictionary which contains all ncessary arguments for the running of your function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "appended run runner-0\n",
      "appended run runner-1\n",
      "appended run runner-2\n"
     ]
    }
   ],
   "source": [
    "runs = [[21, 2],\n",
    "        [64, 8],\n",
    "        [10, 7]]\n",
    "\n",
    "for run in runs:\n",
    "    \n",
    "    a = run[0]\n",
    "    b = run[1]\n",
    "    \n",
    "    arguments = {'a': a, 'b': b}\n",
    "    \n",
    "    ds.append_run(arguments=arguments)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Running and Retrieving your results\n",
    "\n",
    "Now we have created a dataset and appended some runs, we can launch the calculations. This is done via the Dataset.run() method\n",
    "\n",
    "Once the runs have completed, you can retrieve your results with `ds.fetch_results()`, and access them via `ds.results` once this is done\n",
    "\n",
    ".. note::\n",
    "    Be aware that the `fetch_results` method does not return your results, simply stores them in the `results` property."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "assessing run for runner dataset-86f42fea-runner-0... checks passed, running\n",
      "assessing run for runner dataset-9a67082f-runner-1... checks passed, running\n",
      "assessing run for runner dataset-0587be17-runner-2... checks passed, running\n"
     ]
    }
   ],
   "source": [
    "ds.run()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "checking remotely for finished runs\n",
      "None\n"
     ]
    }
   ],
   "source": [
    "# fetch the results, this loads them into the ds.results property for later access\n",
    "import time\n",
    "\n",
    "time.sleep(3)\n",
    "results = ds.fetch_results()\n",
    "print(results)  # This will print None, as fetch_results does not return anything"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[42, 512, 70]\n"
     ]
    }
   ],
   "source": [
    "# access this property any time after the results have been fetched. \n",
    "# This prevents the dataset attempting to poll the remote each time\n",
    "\n",
    "print(ds.results)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With this, you have all of the basic tools available to run python functions on a remote machine. See the other tutorials for more advanced usage\n",
    "\n",
    ".. warning::\n",
    "    Be aware that on MacOS, you may receive some errors when transferring data. This is most likely due to MacOS natively using an `rsync` version below 3, which does not support the argument expansion syntax used heavily in the Transport module. A workaround for this in planned, however for now you should attempt to update your rsync to a version >3 if possible, or use a different transport system (such as scp)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
